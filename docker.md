## Docker的文档：

* 从入门到实践 [https://github.com/yeasy/docker\_practice](https://github.com/yeasy/docker_practice) 

运行docker不加sudo

```
# 第一步
sudo groupadd docker

# 第二步
sudo gpasswd -a vagrant docker

# 第三步
group docker

# 第四步
docker version

# 第五步
sudo service docker restart

# 第六步
exit

# 第七步
vagrant ssh
```



