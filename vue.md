### Vue库

* [better-scroll](https://github.com/ustbhuangyi/better-scroll) inspired by iscroll, and it supports more features and has a better scroll perfermance
* [Vue-Layout](https://github.com/jaweii/Vue-Layout) 基于UI组件的Vue可视化布局工具
* [h5maker](https://github.com/zhengguorong/h5maker) h5编辑器类似maka、易企秀

### Vue文章

* [awesome-vue](https://github.com/vuejs/awesome-vue)

* [vue组件内script格式化自动加"和;](https://github.com/vuejs/vetur/issues/483)

* [Vue.js 技术揭秘](https://ustbhuangyi.github.io/vue-analysis/)

* [Vue技术内幕](http://hcysun.me/vue-design/)

### 工具

* [vue组件内script格式化自动加"和;](https://github.com/vuejs/vetur/issues/483)



